const api = {
    key: "481e88296207ab5d85cde4453121b43c",
    base: "https://api.openweathermap.org/data/2.5/"
}
 
  const search = document.querySelector(".search");
  const btn =document.querySelector(".btn1");
  btn.addEventListener("click",getInput);

  function getInput(event){
      event.preventDefault();
      if(event.type == "click"){
          getData(search.value);
          console.log(search.value);
      }
  }
  function getData(){
    fetch(`${api.base}weather?q=${search.value}&units=metric&APPID=${api.key}`)
    .then(weather => {
      return weather.json();
    }).then(displayResults);
  }

  function displayResults (weather) {
    let city = document.querySelector('.location .city');
    city.innerText = `${weather.name}, ${weather.sys.country}`;
  
    let now = new Date();
    let date = document.querySelector('.location .date');
    date.innerText = dateBuilder(now);
  
    let temp = document.querySelector('.current .temp');
    temp.innerHTML = `${Math.round(weather.main.temp)}<span>°c</span>`;

    let weather_element = document.querySelector('.current .weather');
    weather_element.innerText = weather.weather[0].main;

   
  }

  var images = new Array()
  images[0] = "img1.jpg";
  images[1] = "img2.jpg";
  images[2] = "img3.jpg";
  setInterval("changeImage()", 30000);
  var x=0;

  function changeImage()
  {
             document.getElementById("img").src=images[x]
             x++;
             if (images.length == x) 
             {
                 x = 0;
             }
  }

  function dateBuilder (d) {
       let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
       let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
       let day = days[d.getDay()];
        let date = d.getDate();
        let month = months[d.getMonth()];
        let year = d.getFullYear();
  
    return `${day} ${date} ${month} ${year}`;
  }
  
